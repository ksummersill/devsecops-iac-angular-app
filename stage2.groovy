pipeline {
    agent any
    environment {
        environment = 'testing'
    }
    stages {
        stage('Compile Application') {
            steps {
                sh 'npm install'
                sh 'npm run ng build'
                sh 'npm install --save-dev "jasmine-core@>=3.5"'
            // sh 'export CHROME_BIN=/usr/bin/google-chrome'
            }
        }
        stage ('Testing') {
            steps {
                sh 'ng test --code-coverage --browsers=ChromeHeadlessNoSandbox --watch=false'
            }
        }
        //        stage ('Code Analysis') {
        //          steps {
        //            sh 'npm run sonar'
        //          echo 'Quality analysis......'
        //    }
        stage('SonarQube analysis') {
            //def scannerHome = tool 'SonarScanner 4.0';
           // environment {
           //     scannerHome = tool 'sonar'
            steps {
                withSonarQubeEnv('sonar') {
                    //sh 'npm run sonar'
                    //sh 'sonar-scanner -X'
                    echo "in the sonarqube step"
                    sh   "/var/jenkins_home/workspace/Angular9-CI/node_modules/sonar-scanner/bin/sonar-scanner"
                    echo "after the sonarqube step"
                    //sh "${scannerHome}/bin/sonar-scanner"
                    /* groovylint-disable-next-line NestedBlockDepth */
                timeout(time: 10, unit: 'MINUTES') {
                    /* groovylint-disable-next-line SpaceAroundMapEntryColon */
                    waitForQualityGate abortPipeline: true
                }
                }
            }
        }
        }
    }
