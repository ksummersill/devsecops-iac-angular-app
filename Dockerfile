FROM node:current-alpine3.12 as build-binary
COPY /app /app
WORKDIR /app
RUN npm install
RUN npm run ng build
RUN ls

FROM centos
RUN yum install httpd -y
COPY --from=build-binary /app/dist/devsecops-iac-angular-app/ /var/www/html/
EXPOSE 80
ENTRYPOINT ["/usr/sbin/httpd"] 
CMD ["-D", "FOREGROUND"]